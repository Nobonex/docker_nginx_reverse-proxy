# Simple setup for an nginx server with reverse proxy.

This repository contains a simple dockerised way of setting up a NGINX server with reverse proxy.
For convenience sake I have also included some example images I use regularly.

---

### How to use

Go to the nginx-proxy folder and run : `docker-compose up -d`

To use any of the examples, first open them in your preferred editor and change the following fields
```
VIRTUAL_HOST
VIRTUAL_PORT
LETSENCRYPT_HOST
LETSENCRYPT_EMAIL
```

All the docker-compose files from the examples have different names, for example docker-compose-gitlab.yml .
To run it, use `docker-compose -f docker-compose-gitlab.yml up -d`

Specific instructions for the examples can be found below.

#### Sonarqube example

The Sonarqube example has some pitfalls.
When using the Sonarqube example you must change the last volume path to an explicit mapping.

#### Jenkins exapmle

The Jenkins docker image has some strange issues as it can't write to the folders mapped to it without being granted some user's privileges.
By default the docker-compose file specifies the root user.

#### Gitlab example

The Gitlab docker image is a great 1 way tool to quickly set up your own git service. The docker-compose file I supplied does not specify any settings, meaning it will use the default ones. <br/> Go [here](https://docs.gitlab.com/omnibus/docker/) for more information.
<br/>Do note that when you specify settings in the `environment` part of the docker-compose file it will **always** use those settings, even if you change them in the files stored in you linked folder.

For the Gitlab example you must also change the `hostname` field in the docker-compose file.

